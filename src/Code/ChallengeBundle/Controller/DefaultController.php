<?php

namespace Code\ChallengeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
	/**
	 * @Route("/", name="homepage")
	 * 
	 */
    public function indexAction()
    {
        return $this->render('CodeChallengeBundle:Default:index.html.twig');
    }
}
