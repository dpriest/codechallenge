<?php

namespace Code\ChallengeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CodeChallengeBundle\Model\Books;
use CodeChallengeBundle\Model\BooksQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class BookShelfController extends Controller
{

    /**
     * @Route("/bookshelf", name="bookshelf")
     * 
     */
    public function indexAction()
    {
        $books = BooksQuery::create()->find();
        return $this->render('CodeChallengeBundle:BookShelf:index.html.twig', array('books' => $books));    
    }

    /**
     * @Route("/bookshelf/form/{1}", name="bookShelfForm")
     * 
     */
    public function formAction(Request $request,$id=null)
    {
       
        // print_r($request);
        
       
        $book = new Books;

        if($id){
            $bookQuery = BooksQuery::create()->findpk($id);
            // print_r($bookQuery);
            
            $form = $this->createFormBuilder($book)
                ->add('name', 'text',array('data' => $bookQuery->getName()))
                ->add('author', 'text',array('data' => $bookQuery->getAuthor()))
                ->add('save', 'submit', array('label' => 'Save'))
                ->getForm();

            $form->setData($bookQuery);

                return $this->render('CodeChallengeBundle:BookShelf:form.html.twig', array(
            'form' => $form->createView()
            ));  
        } else {

            $form = $this->createFormBuilder($book)
                ->add('name', 'text',array('data' =>"author test"))
                ->add('author', 'text',array('data' =>"author test"))
                ->add('save', 'submit', array('label' => 'Save'))
                ->getForm();


            // $form->handleRequest($request);
            // if($form->isValid() && $form->isSubmitted()){
            //     $book->save();
            //     }
                

            }
            $form->handleRequest($request);
            if($form->isValid() && $form->isSubmitted()){
                $book->save();
                }
                return $this->redirectToRoute('bookshelf_form');
          
    }

    
    /**
     * @Route("/bookshelf/create", name="createBook")
     * 
     */
    public function createAction()
    {
        $book = new Books;
        $book->setName("Symfony 2 CookBook");
        $book->setAuthor("Symfony Team");
        $book->save();

        return $this->redirectToRoute("bookshelf_index");
        
        return $this->render('CodeChallengeBundle:BookShelf:create.html.twig', array());    
    
    }

    /**
     * @Route("/bookshelf/read/{id}", name="readBook")
     * 
     */
    public function readAction($id)
    {
        $book = BooksQuery::create()->findpk($id);

        if(!$book) {
            throw $this->createNotFoundException('No book found for id ' . $id);
        }
        return $this->render('CodeChallengeBundle:BookShelf:read.html.twig', array('book' => $book
                // ...
            ));   
    }

    /**
     * @Route("/bookshelf/update/{id}", name="updateBook")
     * 
     */
    
    public function updateAction($id)
    {
        $book = BooksQuery::create()->findpk($id);

        if(!$book){
            throw $this->createNotFoundException("No book found for id " . $id);
        }

        $book->setName("Php rocks");
        $book->setAuthor("David");
        $book->save();
        return $this->redirectToRoute("bookshelf_index");
        // return $this->render('CodeChallengeBundle:BookShelf:update.html.twig', array());    
    }

    /**
     * @Route("/bookshelf/delete/{id}", name="deleteBook")
     * 
     */
    public function deleteAction($id)
    {
        $book = BooksQuery::create()->findpk($id);
        $book->delete();

        return $this->redirectToRoute("bookshelf_index");

        // return $this->render('CodeChallengeBundle:BookShelf:delete.html.twig', array());    
    }

}
